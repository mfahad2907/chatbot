from flask import Flask, render_template , url_for , flash , redirect , request 
import os
import sys


sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

from gpt4free import you

app = Flask(__name__)
app.config['SECRET_KEY'] = 'cf72cbb64164138f365da070e855394a'

chat = []
def get_answer(question: str) -> str:
    # Set cloudflare clearance cookie and get answer from GPT-4 model
    try:
        result = you.Completion.create(prompt=question , chat=chat)
        chat.append({"question": question, "answer": result.text})
        return result.text.replace('```' , '    ')

    except Exception as e:
        # Return error message if an exception occurs
        print (
            f'An error occurred: {e}. Please make sure you are using a valid cloudflare clearance token and user agent.'
        )

@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        code =  request.form['code']
        question_text_area = str(code)
        answer = get_answer(question_text_area)
        escaped = answer.encode('utf-8').decode('unicode-escape')
        print(escaped)
        print(chat)
        return render_template('home.html', data=escaped)
    return render_template('home.html' )


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")

